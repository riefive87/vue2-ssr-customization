import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './route-an-index'

const { isNavigationFailure, NavigationFailureType } = VueRouter
const originalPush = VueRouter.prototype.push

VueRouter.prototype.push = function push(location) {
    return originalPush.call(this, location).catch((err) => {
        if (err.name !== 'NavigationDuplicated') throw err
        if (isNavigationFailure(err, NavigationFailureType.redirected)) throw err
    })
}

Vue.use(VueRouter)

export function createRouter() {
    const router = new VueRouter({
        mode: 'history',
        base: process.env.BASE_URL,
        routes: routes.map(route => ({
            path: route.path,
            name: route.name,
            meta: route.meta,
            component: route.component
        }))
    })

    return router
}
