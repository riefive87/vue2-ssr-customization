import { loadAsyncComponents } from '@akryum/vue-cli-plugin-ssr/client'
import { createApp } from './main'

createApp({
    async beforeApp({ router }) {
        await loadAsyncComponents({ router })
    },
    afterApp({ app, router, store }) {
        router.beforeEach((to, from, next) => {
            // middleware auth client
            const existsAuth = to.matched.some(record => record.meta.auth)
            const token = store.getters['user/getToken']

            const loggedIn = (!token || token.trim() === '' ? false : true) || store.state.user.loggedIn
            const fullName = to.fullPath
            
            if (existsAuth && !loggedIn) {
                const query = fullName === '/' ? {} : { redirectTo: fullName.replace('/', '') }
                router.push({ path: '/login', query })
            } else if (loggedIn && ['Login', 'Register'].includes(to.name)) {
                router.push({ path: '/' })
            } else {
                next()
            }
        })

        router.onReady(() => {
            app.$mount('#app')
        })
    }
})
