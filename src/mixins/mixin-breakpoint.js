import { screenOrientation } from '@/libraries/util-dom'

export default {
    watch: {
        '$mq'(value) {
            this.$store.dispatch('breakpoint/updateAlias', value)
        }
    },
    beforeDestroy() {
        if (typeof window === 'undefined') { return false }
        window.removeEventListener('resize', this.onResize, { passive: true })
    },
    mounted() {
        this.onResize()
        window.addEventListener('resize', this.onResize, { passive: true })
    },
    methods: {
        onResize() {
            const width = window.innerWidth
            const height = window.innerHeight

            let name = null
            if (width < 600) {
                name = 'xs'
            } else if (width >= 600 && width < 960) {
                name = 'sm'
            } else if (width >= 960 && width < 1264) {
                name = 'md'
            } else if (width >= 1264 && width < 1904) {
                name = 'lg'
            } else if (width >= 1904) {
                name = 'xl'
            }

            const viewName = ['xs', 'sm'].includes(name) ? 'mobile-view' : 'desktop-view'
            this.$store.dispatch('breakpoint/updateName', name)
            this.$store.dispatch('breakpoint/updateOrientation', screenOrientation())
            this.$store.dispatch('breakpoint/updatePixels', { width, height })
            this.$store.dispatch('layout/updateName', viewName)
        }
    }
}
