import { createApp } from './main'
import { createUrl } from './libraries/util-common'
import { decodedText } from './libraries/util-crypt'
import { get } from './libraries/util-object'

export default context => {
    return new Promise(async (resolve, reject) => {
        const { app, router, store } = await createApp()

        const meta = app.$meta()
        context.meta = meta
        const { fullPath } = router.resolve(context.url).route
        
        if (fullPath !== context.url) {
            return reject({ url: fullPath })
        }
        
        router.push(context.url).catch((err) => {
            if (err.name === 'NavigationDuplicated') { return router.currentRoute }
            throw err
        })

        function beforeRouteEach(context, to) {
            // middleware auth server
            const existsAuth = to.matched.some(record => record.meta.auth)
            const cookies = context.cookies || get(context, 'req.cookies', null)
            const shield = get(cookies, 'shield', '').toString().replace(/\"/g, '')
            const token = decodedText(shield, get(cookies, 'guard'))

            console.log(Object.keys(context))
            console.log(context.req.cookies)

            const loggedIn = (!token || token.trim() === '' ? false : true)
            const fullName = to.fullPath

            if (existsAuth && !loggedIn) {
                const query = fullName === '/' ? {} : { redirectTo: fullName.replace('/', '') }
                return { path: '/login', query }
            } else if (loggedIn && ['Login', 'Register'].includes(to.name)) {
                return { path: '/' }
            }
            return null
        }

        router.onReady(() => {
            const matchedComponents = router.getMatchedComponents()
            if (!matchedComponents.length) { return reject({ code: 404, url: context.url }) }
            
            context.rendered = () => {
                // After all preFetch hooks are resolved, our store is now
                // filled with the state needed to render the app.
                // When we attach the state to the context, and the `template` option
                // is used for the renderer, the state will automatically be
                // serialized and injected into the HTML as `window.__INITIAL_STATE__`.
                context.state = store.state
            }
            
            const to = router.currentRoute
            const nextRoute = beforeRouteEach(context, to)
            if (nextRoute) {
                const nextUrl = createUrl(nextRoute, true)
                context.redirect = { url: nextUrl.stringify }
                reject({ code: 304, url: nextUrl.stringify, stack: 'redirect failed in development mode' })
            } else {
                resolve(app)
            }
        }, reject)
    })
}
