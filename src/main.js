import Vue from 'vue'
import App from './App.vue'
import { createRouter } from './router'
import { createStore } from './store'
import i18n from './locales'
import MixinGlobal from './mixins/mixin-global'

// add plugins
import './plugins/plugin-breakpoint.js'
import './plugins/plugin-meta.js'

// add styles
import '@/assets/style-preload.scss'
import '@/assets/style-ellipsis.css'
import '@/assets/style.css'
import '@mdi/font/css/materialdesignicons.min.css'

Vue.config.productionTip = false
Vue.mixin(MixinGlobal)

export async function createApp ({
    beforeApp = () => {},
    afterApp = () => {}
} = {}) {
    const router = createRouter()
    const store = createStore()
    const options = { router, store, i18n }

    await beforeApp(options)

    const app = new Vue({
        router,
        store,
        i18n,
        render: h => h(App)
    })

    const result = Object.assign({ app }, options)
    await afterApp(result)
    return result
}