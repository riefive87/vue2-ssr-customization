export default {
    namespaced: true,
    state: {
        name: 'desktop-view', // desktop-view | mobile-view
        renderAs: 'client', // client | server
    },
    mutations: {
        changeName(state, value) {
            state.name = value
        },
        changeRender(state, value) {
            state.renderAs = value
        }
    },
    actions: {
        updateName({ commit }, value) {
            commit('changeName', value)
        },
        updateRender({ commit }, value) {
            commit('changeRender', value)
        }
    }
}
