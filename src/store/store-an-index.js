import Cookies from 'js-cookie'
import VuexPersistence from 'vuex-persist'
import ModuleBreakpoint from './store-breakpoint'
import ModuleLayout from './store-layout'
import ModuleUser from './store-user'
 
let plugins = []
if (process.browser) {
    const localStorage = window.localStorage
    const vuexLocalSetup = new VuexPersistence({
        key: 'setup',
        storage: localStorage,
        modules: ['breakpoint', 'layout']
    })
    const vuexLocalUser = new VuexPersistence({
        key: 'shield',
        storage: {
            getItem(key) {
                return localStorage.getItem(key)
            },
            setItem(key, value) {
                return localStorage.setItem(key, value)
            },
            removeItem(key) {
                return localStorage.removeItem(key)
            }
        },
        reducer: (state) => state.user.token
    })
    const vuexLocalTest = new VuexPersistence({
        key: 'test',
        storage: localStorage,
        modules: ['user']
    })
    plugins.push(vuexLocalSetup.plugin)
    plugins.push(vuexLocalUser.plugin)
    plugins.push(vuexLocalTest.plugin)
}
if (process.browser || process.server) {
    const vuexCookieUser = new VuexPersistence({
        key: 'shield',
        storage: {
            getItem(key) {
                return Cookies.get(key)
            },
            setItem(key, value) {
                return Cookies.set(key, value, { expires: 3 })
            },
            removeItem(key) {
                return Cookies.remove(key)
            }
        },
        reducer: (state) => state.user.token
    })
    plugins.push(vuexCookieUser.plugin)
}

const storeModules = {
    breakpoint: ModuleBreakpoint,
    layout: ModuleLayout,
    user: ModuleUser
}
const storePlugins = plugins

export { storeModules, storePlugins }
