import cookies from 'js-cookie'
import { encodedText, decodedText } from '../libraries/util-crypt'

export default {
    namespaced: true,
    state: {
        loggedIn: false,
        remember: false,
        secret: null,
        token: null
    },
    mutations: {
        changeLogin(state, value) {
            state.loggedIn = value
        },
        changeRemember(state, value) {
            state.remember = value
        },
        changeSecret(state, value) {
            state.secret = value
        },
        changeToken(state, value) {
            state.token = value
        }
    },
    actions: {
        generateKey({ commit, getters }) {
            const randomText = Math.random().toString(36).substring(2)
            const secret = getters.getKey
            let isChange = false
            if (!secret || secret.toString() === 'null' || secret.toString().trim() === '') {
                cookies.set('guard', randomText, { expires: 3 })
                isChange = true
            }
            if (process.browser) {
                const expiredPrev = window.localStorage.getItem('expired') || new Date().toISOString()
                const expiredPrevDate = new Date(expiredPrev)
                const today = new Date()
                const expiredNewDate = new Date()
                expiredNewDate.setDate(today.getDate() + 3)
                if (expiredPrevDate.getTime() - today.getTime() <= 0) {
                    window.localStorage.setItem('guard', randomText)
                    window.localStorage.setItem('expired', expiredNewDate.toISOString())
                    cookies.set('guard', randomText, { expires: 3 })
                    isChange = true
                }
            }
            if (isChange) {
                commit('changeSecret', randomText)
            }
        },
        updateLogin({ commit }, value) {
            commit('changeLogin', value)
        },
        updateRemember({ commit }, value) {
            commit('changeRemember', value)
        },
        updateToken({ commit, getters }, value) {
            if (!value) {
                commit('changeToken', null)
                return null 
            } else {
                const newValue = encodedText(value, getters.getKey)
                commit('changeToken', newValue)
            }
        }
    },
    getters: {
        getKey: () => {
            const newKey = (process.browser ? window.localStorage.getItem('guard') : cookies.get('guard')) || ''
            return newKey ? newKey.replace(/\"/g, '') : null
        },
        getToken: (state, getters) => {
            const token = state.token
            if (!token) { return null }
            const newToken = decodedText(token.toString().replace(/\"/g, ''), getters.getKey)
            return newToken
        }
    }
}
