import Vue from 'vue'
import Vuex from 'vuex'
import { storeModules, storePlugins } from './store-an-index'

Vue.use(Vuex)

export function createStore() {
    return new Vuex.Store({
        state() {
            return {
                language: 'en'
            }
        },
        mutations: {
            changeLanguage(state, value) {
                state.language = value
            }
        },
        actions: {
            updateLanguage({ commit }, value) {
                commit('changeLanguage', value)
            }
        },
        modules: storeModules,
        plugins: storePlugins
    })
}
