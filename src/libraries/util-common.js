export function createUrl(params, isReplaced = false) {
    if (!params) { return null }
    const base = 'base' in params ? params.base : 'http://localhost'
    const path = 'path' in params ? params.path : '/'
    const queries = 'query' in params ? params.query : {}
    const url = new URL(`${base}/${path}`.replace(/\/\//g, '/'))

    for (const key in queries) {
        const element = queries[key]
        url.searchParams.set(key, element)
    }

    return { url, stringify: url.toString().replace(isReplaced ? base : '', '') }
}
