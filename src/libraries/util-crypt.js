const privateKey = 'A09091'

function validInputText(text) {
    if (!text) { return false }
    const textNormalized = text.toString().trim()
    if (['', 'null', 'undefined'].includes(textNormalized)) { return false }
    return true
}

export function encodedText(text, additionKey) {
    if (!validInputText(text)) { return '' }
    return text.toString().split('').reverse().join('').concat(privateKey + additionKey)
}

export function decodedText(text, additionKey) {
    if (!validInputText(text)) { return '' }
    return text.toString().replace(privateKey + additionKey, '').split('').reverse().join('')
}
