import Vue from 'vue'
import VueMq from 'vue-mq'

Vue.use(VueMq, {
    breakpoints: {
        xs: 500,
        sm: 768,
        md: 1024,
        lg: 1360,
        xl: 1800
    }, // default breakpoints - customize this
    defaultBreakpoint: 'xs' // customize this for SSR
})
