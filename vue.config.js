const packages = require('./package.json')

module.exports = {
    chainWebpack: webpackConfig => {
        webpackConfig.plugin('html')
        .tap((args) => {
            args[0].title = 'title' in packages ? packages.title : packages.name
            return args
        })
        if (process.env.VUE_CLI_SSR_TARGET === 'client') {
            webpackConfig.entry('app').clear().add('./src/entry-client.js')
            webpackConfig.target('web')
            webpackConfig.devServer.disableHostCheck(true)
            webpackConfig.optimization.splitChunks({ chunks: 'all'}).minimize(true)
        } else {
            webpackConfig.entry('app').clear().add('./src/entry-server.js')
            webpackConfig.target('node')
            webpackConfig.devtool('source-map')
            webpackConfig.output.globalObject('this')
            webpackConfig.output.libraryTarget('commonjs2')
            webpackConfig.optimization.splitChunks(false).minimize(false)
            webpackConfig.plugins.delete('hmr')
            webpackConfig.plugins.delete('preload')
            webpackConfig.plugins.delete('prefetch')
            webpackConfig.plugins.delete('progress')
            webpackConfig.plugins.delete('friendly-errors')
        }
    },
    configureWebpack: {
        performance: {
            hints: process.env.NODE_ENV === 'production' ? 'warning' : false,
            maxEntrypointSize: 512000,
            maxAssetSize: 512000
        }
    },
    pluginOptions: {
        ssr: {
            port: 8080,
            extendServer: app => {
                const cookieParser = require('cookie-parser')
                app.use(cookieParser())
                app.use((err, req, res, next) => {
                    console.log(req.headers)
                    if (err.url) {
                        res.redirect(err.url)
                    } else {
                        next(err)
                    }
                })
            },
            onRender: (res, context) => {
                if ('redirect' in context) {
                    console.log(context.redirect)
                    res.redirect(context.redirect.url)
                }
                process.on('unhandledRejection', (err) => { 
                   console.log(err.toString()) 
                })
            },
            onError: (error) => {
                // Send to error monitoring service
                console.log(error)
            }
        }
    }
}
