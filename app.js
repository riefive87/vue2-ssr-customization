const fs = require('fs')
const path = require('path')
const express = require('express')
const cookieParser = require('cookie-parser')
const { createBundleRenderer } = require('vue-server-renderer')
const { createLog } = require('./apphelper.js')

if (!fs.existsSync('./dist')) {
    console.log('You must build first, typing "yarn ssr:build"')
    process.exit(1)
}

const server = express()
const cwd = process.cwd() || __dirname
const env = process.env.NODE_ENV || 'development'
const port = process.env.PORT || 8080
const target = process.env.VUE_APP_TARGET || 'csr'
const pretty = !['production', 'staging'].includes(env)
const messages = {
    notFound: 'Page not found',
    internalError: 'Internal server error'
}

server.use(cookieParser())
server.use(express.static(path.resolve(cwd, './dist')))

if (target === 'csr') {
    server.get('*', (req, res) => {
        res.status(404).send(messages.notFound)
    })
} else {
    const template = fs.readFileSync('./dist/index.ssr.html', 'utf-8')
        .toString()
        .replace(/\s+\</g, (pretty ? '\n' : '').concat('<'))
        .replace(/\>\s+/g, ''.concat('>').concat(pretty ? '\n' : ''))
        .trim()
    const clientManifest = require('./dist/vue-ssr-client-manifest.json')
    const renderer = createBundleRenderer(require('./dist/vue-ssr-server-bundle.json'), {
        runInNewContext: false,
        template,
        clientManifest
    })
    
    server.get('*', async (req, res) => {
        const context = {
            url: req.url || '/',
            cookies: req.cookies,
            state: {
                title: 'Vue SSR Customization'
            }
        }
        const [err, content] = await renderer.renderToString(context).then(v => [null, v]).catch(e => [e, null])
        if (err) {
            const errString = typeof err === 'object' ? JSON.stringify(err, null, 4) : err.toString()
            createLog(errString, 'ssr-routing')
            if (err.url) {
                res.redirect(err.url)
            } else if (err.code === 404) {
                res.status(404).end(messages.notFound)
            } else {
                res.status(500).end(messages.internalError)
            }
        } else {
            res.end(content)
        }
    })
}

server.listen(port, () => { 
    console.log(`Listening on: ${port} | target: ${target}`)
})

process.on('unhandledRejection', (err) => { 
    createLog(err.toString(), 'unhandled-rejection') 
})

process.on('uncaughtException', (err) => { 
    createLog(err.toString(), 'uncaught-exception') 
})
