process.env.VUE_CLI_BABEL_TRANSPILE_MODULES = true
const presets = process.env.NODE_ENV === 'development' ? ['@vue/app'] : ['@vue/cli-plugin-babel/preset']
module.exports = {
  presets
}
