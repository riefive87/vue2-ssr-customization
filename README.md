# vue2-ssr-customization

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Initialized
- vue add @akryum/ssr

### Running
- development:
$ yarn ssr:serve
- production:
$ yarn ssr:build
$ yarn start

### Rule config router
- Add sub routing with prefix name "route-[name].js"
- route-an-index.js is parent module of router
- ex: route-an-index.js

### Rule config store
- Add sub module at store with prefix name "store-[name].js"
- store-an-index.js is parent module of store
- ex: store-an-index.js, store-breakpoint.js

### Rule config plugins
- Add sub plugin with prefix name "plugin-[name].js"
- ex: plugin-breakpoint.js

### Rule config mixins
- Add sub mixin with prefix name "mixin-[name].js"
- ex: mixin-breakpoint.js

### Libraries
1. Util Object - instead of lodash
- helper for object manipulation
- reference [https://github.com/you-dont-need/You-Dont-Need-Lodash-Underscore]
- available method: get, empty, has, omit, pick, pull
2. Util String
- helper for text manipulation
- available method: capitalize, camelCase, kebabCase, snakeCase, etc...
3. Util Dom
- helper for dom [client side only]
- available method: screenOrientation, toggleFullScreen, userAgentInformation
